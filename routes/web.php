<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@Home');
Route::get('/category/{id}','MainController@NewArrive');
Route::get('/details/{id}','MainController@Details');
Route::get('/st','MainController@Student');
Route::post('/st','MainController@Student');
Auth::routes();
Route::get('/home', 'HomeController@index');



Route::group(['prefix'=>'category'], function() {
Route::get('/add','CategoryController@AddCategory');
Route::post('/add','CategoryController@AddCategoryData');
Route::get('/manage','CategoryController@ManageCategory');
Route::get('/unpublished/{id}','CategoryController@ManageCategoryUnpublished');
Route::get('/published/{id}','CategoryController@ManageCategoryPublished');
Route::post('/delete/{id}','CategoryController@ManageCategoryDelete');
Route::get('/edit/{id}','CategoryController@ManageCategoryEdit');
Route::post('/update/','CategoryController@ManageCategoryUpdate');
});


Route::group(['prefix'=>'manufacturer'], function() {
    Route::get('/add', 'ManufacturerController@AddManufacturer')->name('manufacturer-add');
    Route::post('/save', 'ManufacturerController@SaveManufacturer')->name('manufacturer-save');
    Route::get('/unpublished/{id}', 'ManufacturerController@ManufacturerPublished');
    Route::get('/published/{id}', 'ManufacturerController@ManufacturerUnPublished');
    Route::post('/delete/{id}', 'ManufacturerController@DeleteManufacturer');
    Route::get('/manage', 'ManufacturerController@ManageManufacturer')->name('manufacturer-manage');

    Route::get('/edit/{id}', 'ManufacturerController@EditManufacturer');
    Route::post('/update/', 'ManufacturerController@SaveManufacturerEdit');

});


Route::group(['prefix'=>'product'], function(){
    Route::get('/add','ProductController@AddProduct')->name('product-add');
    Route::post('/new','ProductController@SaveProduct')->name('product-save');
    Route::get('/manage','ProductController@ManageProduct')->name('product-manage');
});