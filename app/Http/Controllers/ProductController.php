<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manufacturer;
use App\Category;
use App\Product;
use DB;
use Image;
class ProductController extends Controller
{
    public function AddProduct()
    {
     /*   elequent */
        $published_category=Category::where('publication_status','1')->get();
      /*  query builder*/
        $published_manufacturer= DB::table('manufacturers')
            ->where('manufacturer_status', 1)->get();
        return view('admin.product.addproduct')->with((['published_category' => $published_category,'published_manufacturer'=>$published_manufacturer]));
    }

    public  function  SaveProduct(Request $request){

        $productImage=$request->file('product_image');
        $fileextention=$productImage->getClientOriginalExtension();
        $uploadpath='public/product_image/';
        $imagename=$request->product_name.'.'.$fileextention;
        $imageurl=$uploadpath.$imagename;
        Image::make($productImage)->resize(270,350)->save('public/product_image/hello.jpg');
        $product= new Product();
        $product->product_name=$request->product_name;
        $product->category_id=$request->category_id;
        $product->manufacturer_id=$request->manufacturer_id;
        $product->product_price=$request->product_price;
        $product->product_quantity=$request->product_quantity;
        $product->product_sdescription=$request->product_sdescription;
        $product->product_fulldescription=$request->product_sdescription;
        $product->product_status=$request->product_status;
        $product->image_url=$imageurl;
        $product->save();
        return view('admin.product.addproduct');
    }


    public  function  ManageProduct(){
        $product= DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
            ->select('products.*', 'categories.category_name', 'manufacturers.manufacturer_name')
            ->get();
        return view('admin.product.manageproduct')->with(['product' => $product]);
    }
}
