<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;


class ManufacturerController extends Controller
{
    private function  decrypte($id){
        $new_id=base64_decode($id);
        $res = preg_replace("/[^0-9,.]/", "", $new_id );
        return $res/256;
    }
    public function AddManufacturer()
    {
        return view('admin.manufacturer.addmanufacturer');
    }

    public function ManageManufacturer()
    {
        $manufacturer = DB::table('manufacturers')->
        get();
        return view('admin.manufacturer.managemanufacturer')->with(['manufacturer' => $manufacturer]);

    }


    public function SaveManufacturer(Request $request)
    {
        $name = $request->manufacturer_status;
        $description = $request->manufacturer_description;
        $status = $request->manufacturer_status;
        DB::table('manufacturers')->insert(
            [
                'manufacturer_name' => $name,
                'manufacturer_description' => $description,
                'manufacturer_status' => $status,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]
        );
        return redirect('/manufacturer/add')->with('message', 'Data Inserted');
    }

    public function ManufacturerUnpublished($id){
        $result=$this->decrypte($id);
        DB::table('manufacturers')
            ->where('id', $result)
            ->update(['manufacturer_status' => 1]);
        return redirect('/manufacturer/manage')->with('message','Data Changed');

    }


    public function ManufacturerPublished($id){
        $result=$this->decrypte($id);
        DB::table('manufacturers')
            ->where('id', $result)
            ->update(['manufacturer_status' => 2 ]);
        return redirect('/manufacturer/manage')->with('message','Data Changed');

    }

    public  function  DeleteManufacturer($id){
        $result=$this->decrypte($id);
        DB::table('manufacturers')->where('id', $result)->delete();
        return redirect('/manufacturer/manage')->with('message','Data Changed');
    }


    public function EditManufacturer($id)
    {
        $result=$this->decrypte($id);
        $manufacturerById = DB::table('manufacturers')->where('id', $result)->first();
        return view('admin.manufacturer.editManufacturer')->with(['manufacturerById' => $manufacturerById]);

    }

    public  function  SaveManufacturerEdit(Request $request){
        DB::table('manufacturers')
            ->where('id', $request->manufacturer_id)
            ->update([
                'manufacturer_name' => $request->manufacturer_name,
                'manufacturer_description' => $request->manufacturer_description,
                'manufacturer_status' => $request-> manufacturer_status,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]);

        return redirect('/manufacturer/manage')->with('message','Data Changed');
    }
}
