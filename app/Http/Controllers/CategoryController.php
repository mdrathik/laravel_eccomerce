<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Category;

class CategoryController extends Controller
{
    public function  AddCategory(){
        return view('admin.category.addcategory');

    }
   public function  AddCategoryData(Request $request){
       /* $data = array();
        $data['category_name']= $request->category_name;
        $data['category_description']= $request->category_description;
        $data['publication_status']= $request->publication_status;

        DB::table('categories')
            ->insert($data);

        echo 'ok';*/
       Category::create($request->all());
       return redirect('/category/add')->with('message','Data Inserted');
    }



    public function ManageCategory(){
        $categories=Category::all();
        return view('admin.category.managecategory',['categories'=>$categories]);
    }

   public function ManageCategoryUnpublished($id){
       $categoryById=Category::find($id);
       $categoryById->publication_status=2;
       $categoryById->save();
       return redirect('/category/manage')->with('message','Data Changed');;
   }


    public function ManageCategoryPublished($id){
        $categoryById=Category::find($id);
        $categoryById->publication_status=1;
        $categoryById->save();
        return redirect('/category/manage')->with('message','Data Changed');
    }


    public function ManageCategoryDelete($id){
        DB::table('categories')->where('id', $id)->delete();
        return redirect('/category/manage')->with('message','Data Changed');;
    }
    public function ManageCategoryEdit($id){
        $categoryById=Category::find($id);
        return view('admin.category.editcategory',['categoryById'=> $categoryById]);
    }


    public function ManageCategoryUpdate(Request $request){
        $category_id=$request->category_id;
        $category=Category::find($category_id);
        $category->category_name=$request->category_name;
        $category->publication_status=$request->publication_status;
        $category->category_description=$request->category_description;
        $category->save();
        return redirect('/category/manage')->with('message','Data Changed');;
    }






}
