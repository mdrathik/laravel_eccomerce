@extends('layouts.app')
@section('content')


    <div class="col-md-12">
        <div class="row">
            <div class="page-header">
                <h1 class="text-center">Add Product</h1>

                <h2 class="text-center text-success">{{Session::get('msg')}}</h2>
            </div>

            <form class="form-horizontal" action="{{route('product-save')}}" method="post" role="form" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">product Name</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control"  name="product_name" placeholder="product Name" required autofocus>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Category Name</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="category_id">
                            @foreach($published_category as $key)
                            <option value="{{$key->id}}">{{$key->category_name}}</option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Manufacturer Name</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="manufacturer_id">
                            @foreach($published_manufacturer as $key)
                                <option value="{{$key->id}}">{{$key->manufacturer_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">product price</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" min="1"  name="product_price" placeholder="product price" required autofocus>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Quantity Name</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" min="1"  name="product_quantity" placeholder="product quantity" required autofocus>
                        <span class="text-danger"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">product short Description</label>
                    <div class="col-sm-6">
                        <textarea id="message" name="product_sdescription" placeholder="product short Description" class="form-control" rows="5" required></textarea>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">product  Description</label>
                    <div class="col-sm-7">
                        <textarea id="message" name="product_fulldescription" placeholder="product Description" class="form-control" rows="10" required></textarea>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Publication Status</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="product_status">
                            <option value="1">Published</option>
                            <option value="2">UnPublished</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Product Image</label>
                    <div class="col-sm-4">
                       <input type="file" placeholder="product)image" name="product_image" accept="image/*">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit"  class="btn btn-primary">Submit Data</button>
                    </div>
                </div>
            </form>

        </div>

    </div>

@endsection