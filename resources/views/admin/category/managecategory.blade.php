@extends('layouts.app')
@section('content')

    <div class="row">

        <div class="col-lg-12">
            <h1 class="text-center">Database</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-center text-success">{{Session::get('message')}}</h4>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Category ID</th>
                            <th>Category Name</th>
                            <th>Category Description</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $key)
                        <tr class="odd gradeX">
                            <td class="center">{{$key->id}}</td>
                            <td>{{$key->category_name}}</td>
                            <td>{{$key->category_description}}</td>
                            @if($key->publication_status==1)
                                <td >
                            published
                            </td>
                            @else
                                <td >
                                unpublished
                                </td>
                            @endif

                            <td class="center">

                             @if($key->publication_status==1)
                                <a href="{{url('category/unpublished/'.$key->id)}}" title="Published" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-arrow-up"></span></a>
                                @else
                               <a href="{{url('category/published/'.$key->id)}}" title="UnPublished" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-arrow-down"></span></a>
                                @endif
                                 <a href="{{url('category/edit/'.$key->id)}}" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-heart"></span></a>
                                <form style="display: inline;" action="{{url('category/delete/'.$key->id)}}" method="post">
                                    {{csrf_field()}}
                                   <button  type="submit"  onClick="return confirm('Delete This account?')"  class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                                </form>
                            </td>
                        </tr>
       @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection
