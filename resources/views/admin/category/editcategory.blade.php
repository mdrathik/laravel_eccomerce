@extends('layouts.app')
@section('content')

        <div class="col-md-12">
    <div class="row">
            <div class="page-header">
                <h1 class="text-center">Edit</h1>
            </div>


            <form class="form-horizontal" action="{{url('category/update/')}}" method="post" role="form">
                {{csrf_field()}}
                <input type="hidden" name="category_id" value="{{$categoryById->id}}">
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Category Name</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control"  name="category_name" value="{{$categoryById->category_name}}" required autofocus>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Publication Status</label>
                    <div class="col-sm-4">
                    <select class="form-control"  name="publication_status">
                        <option value="1">Published</option>
                        <option value="2">UnPublished</option>
                    </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Category Descriptions</label>
                    <div class="col-sm-6">
                        <textarea id="message" name="category_description"  placeholder="" class="form-control" rows="5" required>{{$categoryById->category_description}}</textarea>
                    </div>
                </div>


         <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit" name="btn" class="btn btn-primary">Submit Data</button>
                    </div>
                </div>
            </form>


        </div>

    </div>

@endsection
