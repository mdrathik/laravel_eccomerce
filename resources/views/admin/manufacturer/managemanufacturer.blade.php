@extends('layouts.app')
@section('content')


    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-center">Database</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-center text-success">{{Session::get('message')}}</h4>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>manufacturer ID</th>
                            <th>manufacturer Name</th>
                            <th>manufacturer Description</th>
                            <th>manufacturer Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($manufacturer as $key)

                            <tr class="odd gradeX">
                                <td class="center">{{$key->id}}</td>
                                <td>{{$key->manufacturer_name}}</td>
                                <td>{{$key->manufacturer_description}}</td>
                                @if($key->manufacturer_status==1)
                                    <td>
                                        published
                                    </td>
                                @else
                                    <td>
                                        unpublished
                                    </td>
                                @endif

                                <td class="center">
                                    <?php $new_id=$key->id;?>
                                    @if($key->manufacturer_status==1)
                                        <a href="{{url('manufacturer/unpublished/'.base64_encode($new_id*(256).$key->manufacturer_name) )}}" title="Published"
                                           class="btn btn-success btn-sm"><span
                                                    class="glyphicon glyphicon-arrow-up"></span></a>
                                    @else
                                        <a href="{{url('manufacturer/published/'.base64_encode($new_id*(256).$key->manufacturer_name) )}}" title="UnPublished"
                                           class="btn btn-warning btn-sm"><span
                                                    class="glyphicon glyphicon-arrow-down"></span></a>
                                    @endif
                                        <?php $new_id=$key->id;?>
                                    <a href="{{url('manufacturer/edit/'.base64_encode($new_id*(256).$key->manufacturer_name) )}}" class="btn btn-sm btn-primary"><span
                                                class="glyphicon glyphicon-edit"></span></a>
                                    <form style="display: inline;" action="{{url('manufacturer/delete/'.base64_encode(bin2hex(openssl_random_pseudo_bytes(7)).$key->id.bin2hex(openssl_random_pseudo_bytes(3))))}}"
                                          method="post">
                                        {{csrf_field()}}
                                        <button type="submit" onClick="return confirm('Delete This account?')"
                                                class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
