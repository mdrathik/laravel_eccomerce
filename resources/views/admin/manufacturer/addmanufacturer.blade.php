@extends('layouts.app')
@section('content')


    <div class="col-md-12">
        <div class="row">
            <div class="page-header">
                <h1 class="text-center">Add Manufacturer</h1>

                <h2 class="text-center text-success">{{Session::get('msg')}}</h2>
            </div>

            <form class="form-horizontal" action="{{route('manufacturer-save')}}" method="post" role="form">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Manufacturer Name</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control"  name="manufacturer_name" placeholder="Manufacturer Name" required autofocus>
                        <span class="text-danger"><?php echo $errors->first('manufacturer_name'); ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Publication Status</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="manufacturer_status">
                            <option value="1">Published</option>
                            <option value="2">UnPublished</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Manufacturer Description</label>
                    <div class="col-sm-6">
                        <textarea id="message" name="manufacturer_description" placeholder="Manufacturer Description" class="form-control" rows="5" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit"  class="btn btn-primary">Submit Data</button>
                    </div>
                </div>
            </form>

        </div>

    </div>

@endsection