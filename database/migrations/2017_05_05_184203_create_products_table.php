<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->string('product_name',100);
            $table->integer('category_id');
            $table->integer('manufacturer_id');
            $table->float('product_price',10,2);
            $table->integer('product_quantity');
            $table->text('product_sdescription');
            $table->text('product_fulldescription');
            $table->tinyInteger('product_status');
            $table->text('image_url');
            $table->increments('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
